package dk.lundudvikling.droidworkout.Fragments;

import android.support.v4.app.Fragment;

import dk.lundudvikling.droidworkout.MainActivity;

public class BaseFragment extends Fragment {

    private MainActivity mainActivity;

    public MainActivity getMainActivity() {
        if (mainActivity == null)
            mainActivity = (MainActivity) getActivity();
        return mainActivity;
    }

    public BaseFragment(){
        super();
    }
}
