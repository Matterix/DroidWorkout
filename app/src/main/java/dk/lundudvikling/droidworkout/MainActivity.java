package dk.lundudvikling.droidworkout;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import java.lang.reflect.Field;

import dk.lundudvikling.droidworkout.Fragments.FragmentExercise;
import dk.lundudvikling.droidworkout.Fragments.FragmentMySchedule;
import dk.lundudvikling.droidworkout.Fragments.FragmentProfile;
import dk.lundudvikling.droidworkout.Fragments.FragmentWorkout;
import dk.lundudvikling.droidworkout.Handlers.FragmentHandler;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    private FragmentHandler fragmentHandler;

    // TODO: 16-07-2018 Tilføj Firebase Auth


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        fragmentHandler = new FragmentHandler(this);
        disableShiftMode(bottomNavigationView);
        setOnBottomMenuItemClick();
        bottomNavigationView.setSelectedItemId(R.id.bottom_navigation_exercise);
    }

    @SuppressLint("RestrictedApi")
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    private void setOnBottomMenuItemClick() {
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.bottom_navigation_exercise:
                                fragmentHandler.startTransactionNoBackStack(new FragmentExercise());
                                break;
                            case R.id.bottom_navigation_my_schedule:
                                fragmentHandler.startTransactionNoBackStack(new FragmentMySchedule());
                                break;
                            case R.id.bottom_navigation_profile:
                                fragmentHandler.startTransactionNoBackStack(new FragmentProfile());
                                break;
                            case R.id.bottom_navigation_workout:
                                fragmentHandler.startTransactionNoBackStack(new FragmentWorkout());
                                break;
                        }
                        return true;
                    }
                });
    }
}
