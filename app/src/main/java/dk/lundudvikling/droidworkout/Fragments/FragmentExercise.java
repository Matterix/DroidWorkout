package dk.lundudvikling.droidworkout.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import dk.lundudvikling.droidworkout.Models.Exercise;
import dk.lundudvikling.droidworkout.Models.ExerciseGroup;
import dk.lundudvikling.droidworkout.R;

public class FragmentExercise extends BaseFragment {

    @BindView(R.id.fragment_exercise_recycler_view) RecyclerView mRecyclerView;
    private ArrayList<ExerciseGroup> mExercises = new ArrayList<>();
    private ArrayList<Exercise> mBackExercises = new ArrayList<>();
    private ArrayList<Exercise> mBreastExercises = new ArrayList<>();

    // TODO: 16-07-2018 OnClickRecyclerView --> Go to RecyclerView med de items der passer til gruppe der blev trykket på
    // TODO: 16-07-2018 Find en måde at håndtere database på, firebase??


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_exercise, container, false);
        ButterKnife.bind(this, rootView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBackExercises.add(new Exercise("Dødløft", "Debug"));
        mBreastExercises.add(new Exercise("Bænkpres", "Debug"));
        mBreastExercises.add(new Exercise("Flyes", "Debug"));
        mExercises.add(new ExerciseGroup("Ryg", mBackExercises, R.drawable.image_exercise_back));
        mExercises.add(new ExerciseGroup("Bryst",mBreastExercises, R.drawable.image_exercise_chest));
        mExercises.add(new ExerciseGroup("Biceps", mBreastExercises, R.drawable.image_exercise_biceps));
        mExercises.add(new ExerciseGroup("Triceps", mBreastExercises, R.drawable.image_exercise_triceps));
        mExercises.add(new ExerciseGroup("Ben", mBreastExercises, R.drawable.image_exercise_legs));
        mExercises.add(new ExerciseGroup("Skulder", mBreastExercises, R.drawable.image_exercise_shoulder));
        mExercises.add(new ExerciseGroup("Mave", mBreastExercises, R.drawable.image_exercise_stomach));
        setupAdapter();
    }

    private class ExerciseHolder extends RecyclerView.ViewHolder {
        private TextView tvExerciseName;
        private ImageView ivExerciseImage;

        public ExerciseHolder(View itemView) {
            super(itemView);
            tvExerciseName = itemView.findViewById(R.id.exercise_name);
            ivExerciseImage = itemView.findViewById(R.id.exercise_image);
        }
    }

    private class ExerciseAdapter extends RecyclerView.Adapter<ExerciseHolder> {
        private ArrayList<ExerciseGroup> mExercises;
        public ExerciseAdapter(ArrayList<ExerciseGroup> exercises) {
            mExercises = exercises;
        }

        @Override
        public ExerciseHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View view = inflater.inflate(R.layout.cv_exercise_list, viewGroup, false);
            return new ExerciseHolder(view);
        }
        @Override
        public void onBindViewHolder(ExerciseHolder exerciseHolder, int position) {
            ExerciseGroup exerciseGroup = mExercises.get(position);
            exerciseHolder.tvExerciseName.setText(exerciseGroup.getName());
            exerciseHolder.ivExerciseImage.setImageResource(exerciseGroup.getExerciseDrawable());
        }

        @Override
        public int getItemCount() {
            return mExercises.size();
        }
    }

    private void setupAdapter() {
        if (isAdded()) {
            mRecyclerView.setAdapter(new ExerciseAdapter(mExercises));
        }
    }
}
