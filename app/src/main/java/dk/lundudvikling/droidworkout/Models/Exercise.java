package dk.lundudvikling.droidworkout.Models;

public class Exercise {

    // TODO: 16-07-2018 Dette skal være ExerciseList! (F.eks. Ryg, og så skal den indeholde en liste af exercise)

    private String name;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Exercise(String name, String description) {
        this.name = name;
        this.description = description;
    }
}

