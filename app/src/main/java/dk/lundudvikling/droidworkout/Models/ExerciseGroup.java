package dk.lundudvikling.droidworkout.Models;

import java.util.ArrayList;

public class ExerciseGroup {

    private String name;
    private ArrayList<Exercise> exercises;
    private int exerciseDrawable;

    public int getExerciseDrawable() {
        return exerciseDrawable;
    }

    public void setExerciseDrawable(int exerciseDrawable) {
        this.exerciseDrawable = exerciseDrawable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(ArrayList<Exercise> exercises) {
        this.exercises = exercises;
    }

    public ExerciseGroup(String name, ArrayList<Exercise> exercises, int drawable) {
        this.name = name;
        this.exercises = exercises;
        this.exerciseDrawable = drawable;
    }
}
