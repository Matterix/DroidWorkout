package dk.lundudvikling.droidworkout.Handlers;

import android.support.v4.app.FragmentManager;

import dk.lundudvikling.droidworkout.Fragments.BaseFragment;
import dk.lundudvikling.droidworkout.MainActivity;
import dk.lundudvikling.droidworkout.R;

public class FragmentHandler {
    private FragmentManager fm;

    public FragmentHandler(MainActivity activity){
        fm = activity.getSupportFragmentManager();
    }
    public FragmentManager getFragmentManager(){
        return fm;
    }

    public void startTransactionNoBackStack(BaseFragment fragment) {
        fm.beginTransaction().replace(R.id.mainView, fragment).commitAllowingStateLoss();
    }
    public void startTransactionWithBackSack(BaseFragment fragment){
        fm.beginTransaction().replace(R.id.mainView, fragment).addToBackStack(null).commit();
    }
}
